import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetsupportedPage } from './getsupported';

@NgModule({
  declarations: [
    GetsupportedPage,
  ],
  imports: [
    IonicPageModule.forChild(GetsupportedPage),
  ],
})
export class GetsupportedPageModule {}
