import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MetimePage } from '../metime/metime';
import { GroupsPage } from '../groups/groups';
/**
 * Generated class for the GetsupportedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-getsupported',
  templateUrl: 'getsupported.html',
})
export class GetsupportedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetsupportedPage');
  }

  goToGroups() {
    this.navCtrl.push(GroupsPage);
  }

  goToMetime() {
    this.navCtrl.push(MetimePage);
  }

}
