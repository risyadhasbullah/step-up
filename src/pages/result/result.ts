import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    var message = "";
    var result = "";
    this.result = this.navParams.get("result");
    if (this.result < 13) {
      this.message = "Congratulations! You have a low stress level!"
    }

    if (this.result > 13 && this.result < 27) {
      this.message = "You have a moderate stress level, please consult!"
    }

    if (this.result > 27) {
      this.message = "You have a high perceived stress! Consult immediately!"
    }
    console.log(this.message);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');
  }

  goToHome() {
    this.navCtrl.push(HomePage);
  }

}
