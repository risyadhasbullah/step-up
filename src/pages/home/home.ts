import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SugestionPage } from '../sugestion/sugestion';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToSuggestion() {
    this.navCtrl.push(SugestionPage);
  }

}
