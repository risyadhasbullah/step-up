import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GetsupportedPage } from '../getsupported/getsupported';

/**
 * Generated class for the SugestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sugestion',
  templateUrl: 'sugestion.html',
})
export class SugestionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SugestionPage');
  }

  goToSupport() {
    this.navCtrl.push(GetsupportedPage);
  }

}
