import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SugestionPage } from './sugestion';

@NgModule({
  declarations: [
    SugestionPage,
  ],
  imports: [
    IonicPageModule.forChild(SugestionPage),
  ],
})
export class SugestionPageModule {}
