import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetimePage } from './metime';

@NgModule({
  declarations: [
    MetimePage,
  ],
  imports: [
    IonicPageModule.forChild(MetimePage),
  ],
})
export class MetimePageModule {}
