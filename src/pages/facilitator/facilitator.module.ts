import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacilitatorPage } from './facilitator';

@NgModule({
  declarations: [
    FacilitatorPage,
  ],
  imports: [
    IonicPageModule.forChild(FacilitatorPage),
  ],
})
export class FacilitatorPageModule {}
