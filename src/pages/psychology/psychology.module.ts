import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PsychologyPage } from './psychology';

@NgModule({
  declarations: [
    PsychologyPage,
  ],
  imports: [
    IonicPageModule.forChild(PsychologyPage),
  ],
})
export class PsychologyPageModule {}
