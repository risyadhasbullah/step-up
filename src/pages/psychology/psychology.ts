import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultPage } from '../result/result';
/**
 * Generated class for the PsychologyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-psychology',
  templateUrl: 'psychology.html',
})
export class PsychologyPage {
  data = { a:0 , b:0, c:0, d:0, e:0, f:0, g:0, h:0, i:0, j:0 };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PsychologyPage');
  }

  goToStart() {
    this.data.d = Math.abs(4-this.data.d);
    this.data.e = Math.abs(4-this.data.e);
    this.data.g = Math.abs(4-this.data.g);
    this.data.h = Math.abs(4-this.data.h);
    var sum = 0;
    for( var el in this.data ) {
      if( this.data.hasOwnProperty( el ) ) {
        sum += parseFloat( this.data[el] );
      }
    }
    this.navCtrl.push(ResultPage ,{
    result: sum
    });
  }

}
