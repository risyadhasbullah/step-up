import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StartPage } from '../pages/start/start';
import { RegisterPage } from '../pages/register/register';
import { QuestionnairePage } from '../pages/questionnaire/questionnaire';

import { ChatsPage } from '../pages/chats/chats';
import { GetsupportedPage } from '../pages/getsupported/getsupported';
import { GroupsPage } from '../pages/groups/groups';
import { MetimePage } from '../pages/metime/metime';
import { PsychologyPage } from '../pages/psychology/psychology';
import { SugestionPage } from '../pages/sugestion/sugestion';
import { FacilitatorPage } from '../pages/facilitator/facilitator';
import { ResultPage } from '../pages/result/result';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    StartPage,
    RegisterPage,
    QuestionnairePage,
    ChatsPage,
    GetsupportedPage,
    GroupsPage,
    MetimePage,
    PsychologyPage,
    SugestionPage,
    FacilitatorPage,
    ResultPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    StartPage,
    RegisterPage,
    QuestionnairePage,
    ChatsPage,
    GetsupportedPage,
    GroupsPage,
    MetimePage,
    PsychologyPage,
    SugestionPage,
    FacilitatorPage,
    ResultPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
